const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const invert = require("../invert");

const object = invert(testObject);

console.log(object);