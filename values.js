function values(obj){
    let array =[];
    if(obj){
        array = Object.values(obj);
    }
    return array;
}
module.exports = values;