function defaults(obj,defaultProps){
    if(obj && defaultProps){
        for(var key in defaultProps){
            if(!obj[key]){
                obj[key] = defaultProps[key];
            }
        }
    }
    return obj;
}
module.exports = defaults;