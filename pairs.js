function pairs(obj){
    let list = [];
    if(obj){
        for(var key in obj){
            let pair = [];
            pair.push(key);
            pair.push(obj[key]);
            list.push(pair);
        }
    }
    return list;
}
module.exports = pairs;