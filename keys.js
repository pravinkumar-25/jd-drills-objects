function keys(obj){
    let array = [];
    if(obj){
        array = Object.keys(obj);
    }
    return array;
}

module.exports = keys;