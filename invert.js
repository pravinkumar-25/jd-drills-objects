function invert (obj){
    let invertObj= {};
    if(obj){
        for(var key in obj){
            invertObj[obj[key]]= key;
        }
    }
    return invertObj;
}
module.exports = invert;